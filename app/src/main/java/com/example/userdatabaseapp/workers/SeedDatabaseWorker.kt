package com.example.userdatabaseapp.workers

import android.content.Context
import android.util.JsonReader
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.userdatabaseapp.data.AppDatabase
import com.example.userdatabaseapp.data.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SeedDatabaseWorker(
    context: Context,
    workerParams:WorkerParameters
): CoroutineWorker(context,workerParams) {
    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            val filename = inputData.getString(KEY_FILENAME)
            if(filename != null){
                applicationContext.assets.open(filename).use{inputStream ->
                    JsonReader(inputStream.reader()).use {jsonReader ->
                        val userType = object : TypeToken<List<User>>() {}.type
                        val userList: List<User> = Gson().fromJson(jsonReader,userType)
                        val database = AppDatabase.getInstance(applicationContext)
                        database.userDao().upsertAll(userList)

                        Result.success()
                    }
                }
            } else {
                Log.e(TAG, "Error seeding database - no valid filename")
                Result.failure()
            }
        } catch (ex:Exception) {
            Log.e(TAG, "Error seeding database", ex)
            Result.failure()
        }
    }

    companion object {
        private const val TAG = "SeedDatabaseWorker"
        const val KEY_FILENAME = "USER_DATA_FILENAME"
    }
}