package com.example.userdatabaseapp.data

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val userDao:UserDao
) {

    fun getAllUsers(): List<User> {
        return userDao.getAll()
    }

    fun loadAllByUids(uid:IntArray): List<User>{
        return userDao.loadAllByIds(uid)
    }

    fun findUserByName(firstName:String,lastName:String):User{
        return userDao.findByName(firstName,lastName)
    }

    fun insertUsers(vararg users:User){
        users.forEach {
            userDao.insertAll(it)
        }
    }

    fun deleteUser(user:User){
        userDao.delete(user)
    }
}